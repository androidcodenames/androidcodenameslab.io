# Android codenames
Looking up Android device and model names is hard, because a lot of sources confuse the different names that exists for a particular model. The official list of Google Play certified models by Google is such a large page that it can make your browser freeze. [androidcodenames.gitlab.io](https://androidcodenames.gitlab.io) is a resource friendly way of looking up the different names.

## Dependencies 
* [CSVtoTable](https://github.com/vividvilla/csvtotable)
* [List of Google Play devices by Google](https://storage.googleapis.com/play_public/supported_devices.html) (Warning: large page, which is why this project exists)
* `iconv` for converting the list from UTF-16 to UTF-8

## Limitations
It only includes Android devices from the official Play Store database by Google, so the following are excluded:
* Early Android devices with Android Market
* Android device (clones) based on AOSP
* Devices that run Android only with aftermarket modifications, such as iPhones or Windows Phones

If databases with these phones exist (preferably in CSV format), feel free to open an issue!

## Build
The `-e` flag without arguments removes the export buttons, because those will slow down the page. The source is already public, so nothing is lost.
* `pip3 install csvtotable`
* `wget https://storage.googleapis.com/play_public/supported_devices.csv`
* `iconv -f UTF-16LE -t UTF-8 supported_devices.csv -o supported_devices_utf8.csv`
* `csvtotable -e  supported_devices_utf8.csv public/index.html`

## Manual modifications
The table is generated automatically using `csvtotable`, but needs a few manual edits before publishing it. This could be automated, but since I haven't done that I'm documenting it here for myself for the next update.
* Modify title and caption
* Add `<meta name="viewport" content="width=device-width, initial-scale=1.0">` for responsiveness
* Add `<a class="source-code" href="https://gitlab.com/androidcodenames/androidcodenames.gitlab.io">source</a>` to bottom with the following class added to the CSS:
```
.source-code {
    margin: 20px 40px;
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    text-transform: uppercase;
    font-size: 10px;
}
```
## License
[Google LLC is incorporated in California](https://www.bloomberg.com/profile/company/8888000D:US), United States, and the US does [not recognize database rights](https://en.wikipedia.org/wiki/Database_right#United_States). I am not a lawyer, but I would argue that a list of phones is not a creative work but a database of facts, so it is in the public domain. The remaining of this work is licensed under the MIT License.
